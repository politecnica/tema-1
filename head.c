#include "head.h"

// Cuenta la cantidad de datos ingresados al array
int contarValores(char *arrayArgv)
{
    int i = 0;
    int valor = 0;

    while (arrayArgv[i] != '\0')
    {
        if (arrayArgv[i] == ',')
        {
            valor++;
        }
        i++;
    }
    return valor + 1;
}
// Parsea los valores del string para convertirlo en un array numerico
void parsearArray(char **arrayArgv, int *arrayNumerico)
{
    // variable para delimitar los valores del array
    const char l[2] = ",";
    char *token = strtok(arrayArgv[2], l);

    for (int i = 0; i < longitudArray; i++)
    {
        arrayNumerico[i] = atoi(token);
        token = strtok(NULL, l);
    }
}

// Imprime todos los valores del Array
void imprimirArray(int array[], int i, int f)
{
    for (int i = i; i <= f; i++)
    {
        printf("%d ", array[i]);
    }
    printf("\n");
}

/* Hace la impresion recorriendo el arbol binario por anchura usando los niveles como referencia
 */

void imprimirArbol(int array[])
{
    int nodosPorNivel;
    int ArrayTemporal = longitudArray;
    int tabulacion = nivelesArbol;
    int tabulacionElementos = 0;
    int procesoActual = 0;

    for (int nivelActual = 0; nivelActual < nivelesArbol; nivelActual++)
    {
        tabulacion--;
        nodosPorNivel = pow(2, nivelActual);
        for (int i = 0; i < tabulacion; i++)
        {
            printf("\t");
        }

        for (int i = 0; i < nodosPorNivel; i++)
        {
            printf("Proceso %d", procesoActual);
            printf("\t");
            procesoActual++;
        }
        printf("\n");

        if (nivelActual > 0)
        {
            tabulacionElementos = (ArrayTemporal / 2);
        }
        for (int i = 0; i < tabulacion; i++)
        {
            printf("\t");
        }
        int contador = 0;
        for (int i = 0; i < longitudArray; i++)
        {
            printf("%d", array[i]);
            if (i + 1 < longitudArray)
            {
                printf(",");
            }
            contador++;
            if (contador == tabulacionElementos)
            {
                printf("\t");
                contador = 0;
            }
        }
        if (nivelActual > 0)
        {
            ArrayTemporal = tabulacionElementos;
        }
        printf("\n");
    }
}

/* Hace el mapeo recorriendo el arbol binario por anchura usando los niveles como referencia
 */

void mapeo(int array[])
{
    int nodosPorNivel;
    int ArrayTemporal = longitudArray;
    int tabulacion = nivelesArbol;
    int tabulacionElementos = 0;
    int procesoActual = 0;
    int final = longitudArray;
    int middle;
    int particion = longitudArray;

    for (int nivelActual = 0; nivelActual < nivelesArbol; nivelActual++)
    {
        tabulacion--;
        nodosPorNivel = pow(2, nivelActual);
        middle = 0;
        final = particion;
        for (int i = 0; i < nodosPorNivel; i++)
        {
            printf("Proceso %d: ", procesoActual);
            procesoActual++;
            int inicio = 0;
            for (int i = inicio + middle; i < final & i < longitudArray; i++)
            {
                printf("%d", array[i]);
                if (i + 1 < final)
                {
                    printf(",");
                }
            }
            printf("\n");
            middle += particion;
            final += particion;

        }

        if (nivelActual > 0)
        {
            if ((float)ArrayTemporal / 2 >= (int)(ArrayTemporal / 2))
                tabulacionElementos = (ArrayTemporal / 2) + 1;
            else
                tabulacionElementos = (ArrayTemporal / 2);
        }

        if (nivelActual > 0)
        {
            if ((float)ArrayTemporal / 2 >= (int)(ArrayTemporal / 2))
                ArrayTemporal = tabulacionElementos - 1;
            else
                ArrayTemporal = tabulacionElementos;
        }
        particion /= 2;
    }
}

/* Primera llamada */

void mergeSort(int array[])
{
    sortSubArray(array, 0, longitudArray - 1, 1);
}

/* Algoritmo merge:
Une dos sub array en un solo array estando este ultimo ya ordenado
*/
void merge(int array[], int left, int middle1, int middle2, int right)
{
    int izquierda = left;         // indice del array izquierdo
    int derecha = middle2;        // indice del array derecho
    int temporal = left;          // indice del array temporal
    int tempArray[longitudArray]; // array temporal

    /* controla que la izquierda ni la derecha no sean igual al valor del medio
    esto con el fin de que nunca se crucen */
    while (izquierda <= middle1 && derecha <= right)
    {

        if (array[izquierda] <= array[derecha])
        {
            tempArray[temporal++] = array[izquierda++];
        }
        else
        {
            tempArray[temporal++] = array[derecha++];
        }
    }
    if (izquierda == middle2)
    {
        while (derecha <= right)
        {
            tempArray[temporal++] = array[derecha++];
        }
    }
    else
    {
        while (izquierda <= middle1)
        {
            tempArray[temporal++] = array[izquierda++];
        }
    }
    /* Copia los elementos del array temporal al original
     */
    for (int i = left; i <= right; i++)
    {
        array[i] = tempArray[i];
    }
}
// Ordena el array
void sortSubArray(int array[], int low, int high, int nivelActual)
{
    int i;
    // caso base
    if ((high - low) >= 1)
    {
        int middle1 = (low + high) / 2;
        int middle2 = middle1 + 1;

        /*si el nivel actual del arbol de procesos es mayor que su nivel impuesto por la cantidad de nodos
        ordenacion normal del merge, sin procesos*/
        if (nivelActual + 1 > nivelesArbol)
        {
            sortSubArray(array, low, middle1, nivelActual);
            sortSubArray(array, middle2, high, nivelActual);

            merge(array, low, middle1, middle2, high);
            return;
        }

        // variables que contendran los pid de los hijos izquierdo y derecho
        pid_t lpid, rpid;

        // creacion del proceso hijo izquierdo
        lpid = fork();
        if (lpid < 0)
        {
            perror("Left Child Proc. not created\n");
            _exit(-1);
        }
        else if (lpid == 0)
        {
            /* Si el lpid es 0, corresponde al hijo izquierdo por lo tanto, el hijo izquierdo pasa a ejecutar lo siguiente
            Aumenta la cantidad de nodos en el nivel respectivo */
            array[longitudArray + nivelActual]++;

            /* mientras la cantidad de nodos no sea la maxima, el proceso queda ejecutando este bucle, retrasando la creacion
            de sus procesos hijos */
            while (array[longitudArray + nivelActual] < pow(2, nivelActual) && nivelActual + 1 < nivelesArbol)
                ;

            // se ejecuta la funcion para ordenar el array
            sortSubArray(array, low, middle1, nivelActual + 1);
            _exit(0);
        }
        else
        {

            // genera el nodo hijo derecho
            rpid = fork();
            if (rpid < 0)
            {
                perror("Right Child Proc. not created\n");
                _exit(-1);
            }
            else if (rpid == 0)
            {
                array[longitudArray + nivelActual]++;

                while (array[longitudArray + nivelActual] < pow(2, nivelActual) && nivelActual + 1 < nivelesArbol)
                    ;
                // Cuando llegue a la cantidad maxima de nodos se ordena
                sortSubArray(array, middle2, high, nivelActual + 1);

                _exit(0);
            }
        }

        int status;

        // Esperando por los hijos izquierdo y derecho del nodo actual o proceso padre actual
        waitpid(lpid, &status, 0);
        waitpid(rpid, &status, 0);

        /* Imprimir */

        printf("\nlista ordenada: {");
        array[longitudArray + nivelesArbol]--;
        for (i = low; i <= middle1; i++)
        {
            printf("%i", array[i]);
            if (i != middle1)
            {
                printf(",");
            }
            else
            {
                printf(" }");
            }
        }

        /* Imprimir */

        printf("\nlista ordenada: {");
        array[longitudArray + nivelesArbol]--;
        for (i = middle2; i <= high; i++)
        {
            printf("%i", array[i]);
            if (i != high)
            {
                printf(",");
            }
            else
            {
                printf(" }");
            }
        }

        // Impresion de procesos que luego ordenaran el array
        printf("\n: lista izquierda {");
        array[longitudArray + nivelesArbol]--;
        for (i = low; i <= middle1; i++)
        {
            printf("%i", array[i]);
            if (i != middle1)
            {
                printf(",");
            }
            else
            {
                printf(" }");
            }
        }

        printf(", lista derecha{");
        for (i = middle2; i <= high; i++)
        {
            printf("%i", array[i]);
            if (i != high)
            {
                printf(",");
            }
            else
            {
                printf(" }");
            }
        }

        merge(array, low, middle1, middle2, high);
        printf("⇒");
        printf("{");
        for (i = low; i <= high; i++)
        {
            printf("%i", array[i]);
            if (i != high)
            {
                printf(",");
            }
            else
            {
                printf(" }");
            }
        }
        printf("\n");
    }
}
