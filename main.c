#include "head.h"

int main(int argc, char **argv){
    //variables de memoria compartida
    int shmid;
    key_t key = IPC_PRIVATE;

    //array numerico que apunta a la memoria compartida
    int *arrayNumerico;

    //Definimos la longitud en base al segundo argumento
    longitudArray = contarValores(argv[ 2 ]);
    //Almacena la cantidad de procesos
    numeroProcesos = atoi( argv[ 1 ] );
    //Niveles del arbol segun el numero de procesos
    nivelesArbol = log( numeroProcesos + 1 ) / log( 2 );

    /* Memoria compartida para almacenar el array y compartir entre procesos, tiene un tamaño igual a la longitud del array mas los niveles del
    arbol, ese plus se le asigna para coordinar los procesos */
    size_t SHM_SIZE = (longitudArray + nivelesArbol+1) * sizeof( int );

    //Generamos el segmento de memoria
    if ((shmid = shmget(key, SHM_SIZE, IPC_CREAT | 0666)) < 0)
    {
        perror("shmget");
        _exit(1);
    }

    // asigna a la variable el segmento de memoria
    if ((arrayNumerico = shmat(shmid, NULL, 0)) == (int *) -1)
    {
        perror("shmat");
        _exit(1);
    }
    // carga los numeros en el array
    parsearArray(argv, arrayNumerico);
    arrayNumerico[ longitudArray + nivelesArbol ] = numeroProcesos;

    imprimirArbol(arrayNumerico);

    printf("\n===mapeos===\n");
    mapeo(arrayNumerico);
    printf("\n");

    printf("\n===procesamiento===\n");
    mergeSort(arrayNumerico);

    //imprime el array luego del mergeSort (ordenado)
    imprimirArray(arrayNumerico, 0, longitudArray - 1);

    //elimina los datos de la memoria
    if (shmdt(arrayNumerico) == -1)
    {
        perror("shmdt");
        _exit(1);
    }

    if (shmctl(shmid, IPC_RMID, NULL) == -1)
    {
        perror("shmctl");
        _exit(1);
    }
    printf("\n");
    return 0;
}
